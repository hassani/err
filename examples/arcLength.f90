! To compile this program:
! gfortran  ../src/ansiColor.f90  ../src/err.f90 arcLength.f90  -o arcLength
!
program arcLength
   use err_m
	implicit none
   type(err_t) :: errFlag
   real        :: radius, angle, length

   call err_SetOptions ( halting = .false., DisplayWarning = .true., & 
                         title = '--> arcLength Info:', trace = .true. )  
           
	write(*,'(a)',advance='no') 'Give the radius and the angle (in deg.): '
	read(*,*) radius, angle

   call circularArcLength ( radius, angle, errFlag, length )
   call errFlag%display()
   
   !...
   !...
   !...
      
contains
   subroutine circularArcLength ( radius, angle, flag, length )
      real       , intent(in    ) :: radius, angle
      type(err_t), intent(in out) :: flag
      real       , intent(   out) :: length

      character(len=*), parameter :: HERE = 'circularArcLength'
      real                        :: alpha
           
      length = 0.0
         
      if ( radius < 0.0 ) then
	      flag = err_t ( stat = err_USAGE, where = HERE, msg = 'Negative radius' )
         return
      end if
         
      alpha = abs(angle)
	      
      if ( alpha > 360.0 ) then
         flag = err_t ( stat = err_WARNING, where = HERE, &
                         msg = 'Absolute value of angle exceed 360 degrees' // err_NLT // &
                               'We use its principal measure')

	      alpha = alpha - (nint(alpha)/360) * 360.0
	      if ( alpha == 0.0 ) alpha = 360.0
      end if
         
      length = alpha * radius * atan(1.0) / 45.0
   end subroutine circularArcLength 
   
end program arcLength