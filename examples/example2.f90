! To compile this program:
!    gfortran  -cpp  ../src/ansiColor.f90  ../src/err.f90 example2.f90  -o example2
! or
!    ifort  -fpp  ../src/ansiColor.f90  ../src/err.f90 example2.f90  -o example2
! or
!    nagfor  -fpp  ../src/ansiColor.f90  ../src/err.f90 example2.f90  -o example2
!---------------------------------------------------------------------------------------------

! the file ../src/error.fpp contains the 2 macros "error_TraceNreturn" and "error_SetNreturn"

#include "../src/error.fpp" 

PROGRAM foo
   use err_m
   implicit none

   integer, parameter :: STUPID = 11
!
!- In this example, the error flag is a global variable:
!
   type(err_t) :: flag
   
   integer     :: i
!
!- Disable automatic stop when an error is signaled and set some options:
! 
   call err_SetOptions ( halting = .false.,       & !< Halting mode
                         DisplayWarning = .true., & !< Always print warnings
                         title = '--> foo Info:', & !< A general title
                         trace = .true.           ) !< Print traceback
!
!- Add a predefined error code to the existing set of codes:
!   
   call err_SetErrorCode ( code = STUPID, definition = 'Stupid error' )     

   write(*,'(/,a,/)')'A simple example showing how to use the error handler'
   
   write(*,'(a)')'The program will ask you to enter an integer.'
   write(*,'(a)')'If you enter a non-integer, foo_sub0 will signal an error.'
   write(*,'(a)')'The caller will then display the message and stop the program.'
   write(*,'(a)')'Otherwise, this integer (i) will be passed to foo_sub1 which'
   write(*,'(a)')'will pass it to foo_sub2 and so on up to foo_sub4.'
   write(*,'(a)')'An error message will be reported'
   write(*,'(a)')'- by foo_sub1 if i < 1,'
   write(*,'(a)')'- by foo_sub2 if i < 2,'
   write(*,'(a)')'- by foo_sub3 if i < 3,'
   write(*,'(a)')'- by foo_sub4 if i < 4.'
   write(*,'(a)')'Moreover a warning message is reported by foo_sub4 if i > 10.'
!
!- Ask the user for an integer:
!   
   call foo_sub0 ( i ) ; call flag%Display ( abort = .true. ) ! check the flag, display its
                                                              ! contents if any and stop if
                                                              ! necessary
!
!- Proceed:
!   
   call foo_sub1 ( i ) ; call flag%Display ( ) ! check the flag and display its contents if any
   
   print*,"(we go on anyway, even if there's an error)"
   print*,"(.........................................)"
   print*,"(.........................................)"
   print*,"(.........................................)"
   
!...
!...
!... (the rest of the code)
!...
!...
   
   call flag%Display ( ) ! check the flag and display its contents if any
   
!...
!...
!...

   call err_resume ( title = '--> foo terminates with' )
   
CONTAINS

!=============================================================================================
   SUBROUTINE foo_sub0 ( i )
!=============================================================================================
   integer, intent(out) :: i
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: here = 'foo_sub0'
   integer                     :: iostat
   character(80)               :: iomsg
!---------------------------------------------------------------------------------------------

   write(*,'(/,a)',advance='no')'Please enter an integer: '
   read(*,*,iostat=iostat,iomsg=iomsg) i
   
   if ( iostat /= 0 ) then
   
      ! A read error has occurred. Set the error message:
      flag = err_t ( stat = err_READ, where = here, msg = 'invalid integer' // err_NLT // &
                     'IO-MSG: '//trim(adjustl(iomsg)) )
                     
      ! Let the caller decide what to do (unless err_halting = .true.):
      return
      
   end if
   
   END SUBROUTINE foo_sub0
   
!=============================================================================================
   SUBROUTINE foo_sub1 ( i )
!=============================================================================================
   integer, intent(in) :: i
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter:: here = 'foo_sub1'
!---------------------------------------------------------------------------------------------

   error_SetNreturn ( i<1, flag, err_USAGE, 'foo_sub1', 'your integer must be >= 1' )
   
   !equivalently:
   !if ( i < 1 ) then
   !   flag = err_t ( stat = err_USAGE, where = here, msg = 'your integer must be >= 1')
   !   return
   !end if

   !... do some stuff

   call foo_sub2 ( i ) ;  error_TraceNreturn(flag,here)
   
   !... do some stuff

   END SUBROUTINE foo_sub1

!=============================================================================================
   SUBROUTINE foo_sub2 ( i ) 
!=============================================================================================
   integer, intent(in) :: i
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter:: here = 'foo_sub2'
!---------------------------------------------------------------------------------------------

   if ( i < 2 ) then
      flag = err_t ( stat = err_USAGE, where = here, msg = 'your integer must be >= 2')
      return
   end if

   !... do some stuff

   call foo_sub3 ( i ) ; error_TraceNreturn(flag,here)

   !... do some stuff

   END SUBROUTINE foo_sub2
   
!=============================================================================================
   SUBROUTINE foo_sub3 ( i ) 
!=============================================================================================
   integer, intent(in) :: i
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter:: here = 'foo_sub3'
!---------------------------------------------------------------------------------------------
   
   if ( i < 3 ) then 
      flag = err_t ( stat = err_USAGE, where = here, msg = 'your integer must be >= 3')
      return
   end if

   !... do some stuff

   call foo_sub4 ( i ) ; error_TraceNreturn(flag,here)

   !... do some stuff

   END SUBROUTINE foo_sub3
   
!=============================================================================================
   SUBROUTINE foo_sub4 ( i )
!=============================================================================================
   integer, intent(in) :: i
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter:: here = 'foo_sub4'
!---------------------------------------------------------------------------------------------

   if ( i < 4 ) then
      flag = err_t ( stat = err_USAGE, where = here, msg = 'your integer must be >= 4')
      return  
   else if ( i > 10 ) then
      ! Example of a warning:
      flag = err_t ( stat = err_WARNING, where = here, msg = 'your integer is > 10')
      ! Note: A warning report is not postponed to the caller. 
      !       If err_Dispwarn = .true. : it is immediately displayed and then deleted. 
      !       If err_Dispwarn = .false.: the warning is not displayed nor deleted but its 
      !                                  message will be erased by future errors or warnings.
   end if
   
   !... do some stuff

   error_SetNreturn(i==i,flag,STUPID,'foo_sub4','An absurd test error')
!
!  Equivalent to:
!    if ( i == i ) then
!       flag = err_t ( stat = STUPID, where = here, msg = 'An absurd test error')
!       return  
!    end if
   
   !... do some stuff

   END SUBROUTINE foo_sub4

END PROGRAM foo
