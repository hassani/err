! To compile this program:
!    gfortran  -cpp  ../src/ansiColor.f90  ../src/err.f90 example1.f90  -o example1
! or
!    ifort  -fpp  ../src/ansiColor.f90  ../src/err.f90 example1.f90  -o example1
! or
!    nagfor  -fpp  ../src/ansiColor.f90  ../src/err.f90 example1.f90  -o example1
!---------------------------------------------------------------------------------------------

! the file ../src/error.fpp contains the 2 macros "error_TraceNreturn" and "error_SetNreturn"

#include "../src/error.fpp" 

PROGRAM example1
   
   use, intrinsic :: iso_fortran_env, only: RKIND => real64
   
   use err_m
   
   implicit none
   
   type(err_t) :: flagErr
!
!- Disable automatic stop when an error is signaled and set some options:
! 
   call err_SetOptions ( halting = .false., DisplayWarning = .true., & 
                         title = '--> example1 Info:', trace = .true. )  
!
!- Start my calculations:
!                            
   call mainSub ( flagErr )
!
!- Report any error:
!
   call flagErr%Display ( )
   
   call err_resume ( title = '--> example1 terminates with' )
   
CONTAINS

!=============================================================================================      
   SUBROUTINE mainSub ( flag )
!=============================================================================================      
   type(err_t), intent(out) :: flag
!---------------------------------------------------------------------------------------------
!  Reads the data from a file and performs calculations
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------     
   character(len=*), parameter   :: HERE = 'mainSub'
   real     (Rkind), allocatable :: x(:), y(:), z(:,:)
!---------------------------------------------------------------------------------------------
   
   call readMyFile ( x, y, flag ) ; error_TraceNreturn ( flag, HERE )
   
   call computeSomeThings ( x, y, z, flag ) ; error_TraceNreturn ( flag, HERE )
   
   END SUBROUTINE mainSub

!=============================================================================================         
   SUBROUTINE readMyFile ( x, y, flag )
!=============================================================================================      
   real(Rkind), intent(in out), allocatable :: x(:), y(:)
   type(err_t), intent(in out)              :: flag
!---------------------------------------------------------------------------------------------
!  Reads the data from the file "myFile.txt", allocates x and y
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------   
   character(len=*  ), parameter :: HERE = 'readMyFile', &
                                    MSG = 'Unable to open the file "myFile.txt'
   character(len=512)            :: iomsg
   integer                       :: ui, line = 0, stat = 0
!---------------------------------------------------------------------------------------------

   ! Open the file:
   open ( newunit=ui, file='myFile.txt', status = 'old', iostat = stat, iomsg = iomsg )

   ! If something went wrong during the opening, report it:
   ! (warning: be aware that you cannot use continuation line with the cpp used by gfortran):
   error_SetNreturn ( stat/=0, flag, err_USAGE, HERE, MSG//err_NLT//'IO-MSG: '//trim(iomsg) )

   ! Read the dimensions of x and y and allocate these arrays:
   call readDimAndAllocate ( ui, 'myFile.txt', line, x, y, flag )
   
   ! If something went wrong during this read add "readMyfile" to the trace and return:
   error_TraceNreturn ( flag, HERE )
   
   ! Read the data and store them into x and y:
   call readData ( ui, 'myFile.txt', line, x, y, flag )

   ! If something went wrong during this read add "readMyfile" to the trace and return:   
   error_TraceNreturn ( flag, HERE )
   
   END SUBROUTINE readMyFile

!=============================================================================================      
   SUBROUTINE readDimAndAllocate ( ui, fileName, line, x, y, flag )
!=============================================================================================      
   integer         , intent(in    )              :: ui
   character(len=*), intent(in    )              :: fileName
   integer         , intent(in out)              :: line
   real(Rkind)     , intent(in out), allocatable :: x(:), y(:)
   type(err_t)     , intent(in out)              :: flag   
!---------------------------------------------------------------------------------------------
!  Reads the dimensions of the arrays x and y and allocates them
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*  ), parameter   :: HERE = 'readDimAndAllocate'   
   character(len=1  )              :: char1
   character(len=512)              :: iomsg
   integer                         :: n, stat
!---------------------------------------------------------------------------------------------

   line = 1
   read(ui,'(a)') char1
   line = line + 1   
   read(ui,*,iostat=stat,iomsg=iomsg) n
   
   ! Set a read error if something went wrong and return.
   ! Unfortunately gfortran cpp does not recognize the continuation line symbol ('&')
   ! (ifort and nagfor do) so the "error_SetNreturn" macro cannot be used in the presence
   ! of line breaks
   if ( stat /= 0 ) then
      flag = err_t ( stat = err_READ, where = HERE, msg = 'At line #'//i2a(line)// &
                      ' of the file "'//filename//'"'//err_NLT// 'IO-MSG: '//trim(iomsg) )
      return
   end if
   
   ! Set an error if n < 0 and return:                      
   error_SetNreturn ( n<0, flag, err_USAGE, HERE,'Invalid number of data: '//i2a(n) )
   
   ! Set a warning if n = 0:
   if ( n == 0 ) flag = err_t ( stat = err_WARNING, where = HERE,    &
                                 msg = 'The number of data is 0 (!)' )
                                 
   allocate(x(n), y(n), stat = stat)

   ! Set an error if something went wrong during allocation and return:
   error_SetNreturn ( stat/=0, flag, err_ALLOC, HERE, 'Allocation failure for x and/or y' )
   
   END SUBROUTINE readDimAndAllocate
   
!=============================================================================================      
   SUBROUTINE readData ( ui, fileName, line, x, y, flag )
!=============================================================================================      
   integer         , intent(in    )              :: ui
   character(len=*), intent(in    )              :: fileName
   integer         , intent(in out)              :: line
   real(Rkind)     , intent(in out), allocatable :: x(:), y(:)
   type(err_t)     , intent(in out)              :: flag   
!---------------------------------------------------------------------------------------------
!  Reads the arrays x and y
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*  ), parameter :: HERE = 'readData'   
   character(len=1  )            :: char1
   character(len=100)            :: iomsg
   integer                       :: i, stat   
!---------------------------------------------------------------------------------------------
   
   read(ui,'(a)') char1
   do i = 1, size(x)
      line = line + 1
      read(ui,*,iostat=stat,iomsg=iomsg) x(i),y(i)
      if ( stat /= 0 ) exit
   end do

   if ( stat /= 0 ) then
      flag = err_t ( stat = err_READ, where = HERE, msg = 'At line #'//i2a(line)// &
                     ' of the file "'//filename//'"'//err_NLT// 'IO-MSG: '//trim(iomsg) )
      return
   end if
                      
   END SUBROUTINE readData

!=============================================================================================      
   SUBROUTINE computeSomeThings ( x, y, z, flag )
!=============================================================================================      
   real(Rkind), intent(in    )              :: x(:), y(:)
   real(Rkind), intent(   out), allocatable :: z(:,:)
   type(err_t), intent(in out)              :: flag   
!---------------------------------------------------------------------------------------------
!  Computes z = f(x,y)
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*  ), parameter :: HERE = 'computeSomeThings'   
   integer                       :: i, j, stat   
!---------------------------------------------------------------------------------------------
   
   allocate(z(2,size(x)), stat = stat)
   error_SetNreturn ( stat/=0, flag, err_ALLOC, HERE,'Allocation failure for z' )
   
   do i = 1, size(x)
      z(:,i) = funcf ( x(i), y(i), flag )
      if ( flag > 0 ) then
         j = i
         exit
      end if
   end do
   
   if ( flag > 0 ) then
      ! Example of adding further information to an existing message:
      call flag%addMsg ( 'Error occured for index '//i2a(j)//' in the main loop of '//HERE, &
                          newline = .true. )
      ! Add trace and return:
      call flag%addTrace ( HERE )
      return
   end if

   END SUBROUTINE computeSomeThings

!=============================================================================================      
   FUNCTION funcf ( x, y, flag ) result(res)
!=============================================================================================      
   real(Rkind), intent(in    ) :: x, y
   type(err_t), intent(in out) :: flag   
   real(Rkind)                 :: res(2)
!---------------------------------------------------------------------------------------------
!  Computes res(1) = f(x,y) and res(2) = g(res(1))
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*  ), parameter :: HERE = 'funcf'   
!---------------------------------------------------------------------------------------------
   
   res(1) = y - x
   res(2) = funcg ( res(1), flag ) ; error_TraceNreturn ( flag, HERE )

   END FUNCTION funcf   

!=============================================================================================      
   FUNCTION funcg ( xi, flag ) result(res)
!=============================================================================================      
   real(Rkind), intent(in    ) :: xi
   type(err_t), intent(in out) :: flag 
   real(Rkind)                 :: res  
!---------------------------------------------------------------------------------------------
!  Computes res = g(xi)
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*  ), parameter :: HERE = 'funcg'   
!---------------------------------------------------------------------------------------------
   
   error_SetNreturn ( xi <= 0, flag, err_USAGE, HERE, 'Invalid argument (must be positive)' )
   
   res = log(xi)

   END FUNCTION funcg         
   
!=============================================================================================   
   pure FUNCTION i2a ( i ) result( res )
!=============================================================================================
   integer         ,             intent(in) :: i
   character(len=:), allocatable            :: res
!---------------------------------------------------------------------------------------------
!  Converts integer to string (from Vladimir F., https://stackoverflow.com)
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=range(i)+2) :: tmp
!---------------------------------------------------------------------------------------------

   write(tmp,'(i0)') i ;  res = trim(tmp)
  
  END FUNCTION i2a   

END PROGRAM example1