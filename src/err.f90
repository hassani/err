! Copyright 2018-2025  Riad Hassani, Universite Cote d'Azur
!
! The  err_m module is free software: you can redistribute it and/or modify it under the terms
! of  the  GNU  General  Public  License  as published by the Free Software Foundation, either 
! version 3 of the License, or (at your option) any later version.
!
! The err_m module is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
! without  even the implied warranty of  MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE. 
! See the GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License along with the pk2 library.
! If not, see <https://www.gnu.org/licenses/>. 


!---------------------------------------------------------------------------------------------
! Modified from the pk2 library, version 2019.1
!---------------------------------------------------------------------------------------------
!
! Author: R. Hassani, Universite de Nice - Sophia Antipolis
!
! Module: err
!
! Description: defines a useful type for error handling
!---------------------------------------------------------------------------------------------

MODULE err_m

   use, intrinsic :: iso_fortran_env, only: output_unit
   
   use ansiColor_m, only: ansiColor_start   
   
   implicit none

   private
   public :: err_t, err_SetOptions, err_SetErrorCode, err_Resume,               &
             err_GetHaltingMode, err_GetWarningMode, err_MoveAlloc, err_NLT,    &
             err_INTERNAL, err_USAGE, err_READ, err_ALLOC, err_WARNING, err_EOF   

   character(len=*), parameter :: err_NLT = char(10)//'    ' 
!
!- Some predefined error and warning codes (can be redefined by the user):
!
   integer, parameter :: err_USAGE    = 1, & ! user error
                         err_READ     = 2, & ! read error
                         err_ALLOC    = 3, & ! allocation/deallocation error
                         err_INTERNAL = 4, & ! internal error
                         err_WARNING  =-1, & ! warning
                         err_EOF      =-2    ! end-of-file warning
!
!- A DT defining a flag (for passing message error or warning)
!
   type :: err_t
      private
      integer                       :: code = 0
      
      character(len=:), allocatable :: mesg,  &
                                       trace, &
                                       loc
                                       
      logical                       :: has_mesg  = .false., &
                                       has_trace = .false., &
                                       has_loc   = .false.                                       
   contains
      
      procedure :: Set      => err_Set
      procedure :: Display  => err_DisplayErr 
      procedure :: Destroy  => err_DestroyErr 
      procedure :: AddTrace => err_AddTrace 
      procedure :: AddMsg   => err_AddMsg 
      !final :: err_FinalizeErr            
!
!-    Operator overload:
!      
      procedure, pass(a) :: err_Gt
      procedure, pass(a) :: err_GtIntg
      procedure, pass(a) :: err_IntgGt
      generic, public :: operator(>) => err_Gt, err_GtIntg, err_IntgGt 

      procedure, pass(a) :: err_Ge
      procedure, pass(a) :: err_GeIntg
      procedure, pass(a) :: err_IntgGe
      generic, public :: operator(>=) => err_Ge, err_GeIntg, err_IntgGe

      procedure, pass(a) :: err_Lt
      procedure, pass(a) :: err_LtIntg
      procedure, pass(a) :: err_IntgLt
      generic, public :: operator(<) => err_Lt, err_LtIntg, err_IntgLt

      procedure, pass(a) :: err_Le
      procedure, pass(a) :: err_LeIntg
      procedure, pass(a) :: err_IntgLe
      generic, public :: operator(<=) => err_Le, err_LeIntg, err_IntgLe

      procedure, pass(a) :: err_Eq
      procedure, pass(a) :: err_EqIntg
      procedure, pass(a) :: err_IntgEq
      generic, public :: operator(==) => err_Eq, err_EqIntg, err_IntgEq

      procedure, pass(a) :: err_Ne
      procedure, pass(a) :: err_NeIntg
      procedure, pass(a) :: err_IntgNe
      generic, public :: operator(/=) => err_Ne, err_NeIntg, err_IntgNe            
      
   end type err_t 
!
!- Constructor:
!
   interface err_t
      module procedure err_constructor
   end interface
!
!- The default values of the following variables controlling the handler mode can be modified 
!  by the user by calling the "err_SetOptions" subroutine. 
!
!  . err_Halting : controls halting or continuation after an error. 
!                  If err_Halting = .true. (default), the program is stopped as soon as 
!                  the error is set and printed. 
!                  Otherwise, it is the caller responsability to test the returned error code.
!
!  . err_IeeeHalt: controls halting or continuation after an ieee exception (default=.false.)
!
!  . err_DispWarn: if this variable is set to .false. warning messages are ignored (not 
!                  printed). Otherwise, the warning is printed when the message is set.
!
!  . err_trace   : if this variable is set to .false. traceback is not printed when the error
!                  message is displayed.
!
   logical :: err_Halting  = .true.
   logical :: err_IeeeHalt = .false.      
   logical :: err_DispWarn = .true. 
   logical :: err_trace    = .true.
!
!- The following variables can also be modified by calling err_SetOptions: 
! 
!  . err_Unit: unit used to print error and warning message.
!
!  . err_colorError, err_colorWarning, err_colorNormal: colors of the corresponding messages
!    (on stdout)
!
!  . err_title: a default title
!
   integer :: err_Unit = output_unit

   character(len=9) :: err_colorError   = ansiColor_start // '1;91m', & !'r_bh', &
                       err_colorWarning = ansiColor_start // '1;92m', & !'g_bh', &
                       err_colorNormal  = ansiColor_start // '1;94m'    !'b_bh'
   
   character(len=:), allocatable :: err_title
!
!- Count of total number of errors and warnings:
!   
   integer :: err_ncall(2) = 0
!
!- List of error definitions:
!
   character(len=80), allocatable :: err_codes(:)   

CONTAINS

   
!=============================================================================================
   SUBROUTINE err_SetErrorCode ( code, definition, stat )
!=============================================================================================
   integer         , optional, intent(in    ) :: code
   character(len=*), optional, intent(in    ) :: definition
   type     (err_t), optional, intent(in out) :: stat
!---------------------------------------------------------------------------------------------
!  When called for the first time, this subroutine initializes the module array "err_codes" 
!  (error definitions).
!  The user can call this subroutine to replace an existing definition or to add a new one.
!-----------------------------------------------------------------------------------R.H. 12/23      

!- local variables: --------------------------------------------------------------------------
   integer, parameter                         :: Margin = 10, Nmax = 300
   integer                                    :: lb, ub, err
   type(err_t)                                :: flagErr
   character(len=len(err_codes)), allocatable :: tmp(:)
   logical, save                              :: firstime = .true.   
!---------------------------------------------------------------------------------------------

   if ( firstime ) then
!
!-    Predefined codes:
!
      allocate(err_codes(-5:10)) ; err_codes = ''
      err_codes(err_USAGE)    = '--> Error (code '//err_i2a(err_USAGE)//')'
      err_codes(err_READ)     = '--> Read error (code '//err_i2a(err_READ)//')'
      err_codes(err_ALLOC)    = '--> Alloc/Dealloc error (code '//err_i2a(err_ALLOC)//')'
      err_codes(err_INTERNAL) = '--> Internal error (code '//err_i2a(err_INTERNAL)//')'
      err_codes(err_WARNING)  = '--> Warning (code '//err_i2a(err_WARNING)//')'
      err_codes(err_EOF)      = '--> End of file (code '//err_i2a(err_EOF)//')'
      firstime = .false.
   end if
   
   if ( .not. present(code) .and. .not. present(definition) ) return
!
!- Store in "err_codes" the new error/warning definition at index "code" 
!  (first see if "err_codes" need to be resized):
!   
   ub = ubound(err_codes,1) ; lb = lbound(err_codes,1)
   
   if ( code < lb .or. code > ub ) then
!
!-    Resize "err_codes" (take a margin to avoid frequent allocations):
!   
      if ( code < lb ) then
         allocate( tmp(code-Margin:ub), stat=err )
      else if ( code > ub ) then
         allocate( tmp(lb:code+Margin), stat=err ) 
      end if
      
      if ( err == 0 ) then
         tmp = '' ; tmp(lb:ub) = err_codes
         call move_alloc (from = tmp, to = err_codes)
         if ( size(err_codes) > Nmax ) then
            flagErr = err_t ( stat = err_WARNING, where = 'err_SetErrorCode', &
                               msg = 'Large number of codes' )
         end if          
      else      
         flagErr = err_t ( stat = err_ALLOC, where = 'err_SetErrorCode', &
                           msg  = 'Allocation failure for array tmp'     ) 
         if ( .not. present(stat) ) call flagErr%Display ( abort = .true. )
      end if
      
      if ( flagErr /= 0 .and. present(stat) ) then
         call err_moveAlloc ( from = flagErr, to = stat )
         if ( stat > 0 ) return
      end if
   
   end if
   
   err_codes(code) = '--> '//trim(adjustl(definition))//' (code '//err_i2a(code)//')'
   
   END SUBROUTINE err_SetErrorCode
   
      
!=============================================================================================
   SUBROUTINE err_SetOptions ( halting, ieee_halting, DisplayWarning, unit,     &
                               errorColor, warnColor, normalColor, title, trace )
!=============================================================================================
   use, intrinsic :: ieee_arithmetic
   use ansiColor_m, only: ansiColor_getAnsiCode   
   logical         , optional, intent(in) :: halting, ieee_halting, DisplayWarning, trace
   integer         , optional, intent(in) :: unit
   character(len=*), optional, intent(in) :: errorColor, warnColor, normalColor, title
!---------------------------------------------------------------------------------------------
!  This subroutine can be called to modify the default values of the module variables:
!
!  - err_Halting (halting mode), 
!  - err_IeeeHalt (halting mode for ieee exception)
!  - err_DispWarn (warning display)
!  - err_trace (traceback display)
!  - err_Unit (unit used for printing)
!  - err_title (title)
!  - err_colorError, err_colorWarning, err_colorNormal (colors)
!
!  Inputs:
!
!  . halting       : (optional) When present, the given value overwrite the default value of 
!                    the module variable "err_Halting". 
!                    When this value is .true. any invocation of the err_t constructor with
!                    %code > 0 will cause halting. 
!
!  . ieee_halting  : (optional) When present and .true., any ieee exception will cause halting. 
!                    When not present "err_halting" is used.
!
!  . DisplayWarning: (optional) When present, the given value overwrite the default value of 
!                    the module variable "err_DispWarn".
!                    When this value is .true. any invocation of the err_t constructor with
!                    %code < 0 will call %display to print the warning message.
!
!  . unit          : (optional) The unit used to print out error and warning message.
!                    When present, the given value overwrite the default value of the module
!                    variable "err_Unit".
!  
!  . title         : (optional) Set "err_title" (the title to be printed before error/warning 
!                    messages)      
!
!  . trace         : (optional) When present, overwrite the default value of the module 
!                    variable "err_trace".
!
!  . errorColor, warnColor, normalColor : color codes for error, warning and normal messages
!                    (when stdout or sdterr are used).
!                    Theses codes can be ansi codes or aliases in the form: 'c[t]' where c in 
!                    {'k', 'r', 'b', 'y', 'g', 'c', 'm'} (for black, red, blue, yellow, green, 
!                    cyan, magenta) is the color and t in {'b', 'h', 'bh'} (for bold, high,
!                    bold and high) is the texture and/or the intensity. Example : rbh
!
!----------------------------------------------------------------------------R.H. 12/19, 03/22      

!- local variables: --------------------------------------------------------------------------
   character(len=9) :: color
!---------------------------------------------------------------------------------------------
   
   if ( present(halting)        ) err_Halting  = halting
   if ( present(DisplayWarning) ) err_DispWarn = DisplayWarning
   
   if ( present(unit) ) then
      if ( unit /= 5 ) err_Unit = unit
   end if
   
   if ( present(ieee_halting) ) then
      call ieee_set_halting_mode ( ieee_all , halting = ieee_halting)  
   else
      call ieee_set_halting_mode ( ieee_all , halting = err_IeeeHalt ) 
   end if
   
   if ( present(errorColor) ) then
      color = errorColor
      if ( color(1:2) /= ansiColor_start ) color = ansiColor_getAnsiCode ( color )
      if ( len_trim(color) > 0 ) err_colorError = color
   end if

   if ( present(warnColor) ) then
      color = warnColor
      if ( color(1:2) /= ansiColor_start ) color = ansiColor_getAnsiCode ( color )
      if ( len_trim(color) > 0 ) err_colorWarning = color
   end if
   
   if ( present(normalColor) ) then
      color = normalColor
      if ( color(1:2) /= ansiColor_start ) color = ansiColor_getAnsiCode ( color )
      if ( len_trim(color) > 0 ) err_colorNormal = color
   end if

   if ( present(title) ) then
      err_title = title
   else
      err_title = ''
   end if
   
   if ( present(trace) ) err_trace = trace

   END SUBROUTINE err_SetOptions


!=============================================================================================
   SUBROUTINE err_Resume ( unit, reset, title )
!=============================================================================================
   use ansiColor_m, color => ansiColor_colorTxt
   integer         , optional, intent(in) :: unit
   logical         , optional, intent(in) :: reset
   character(len=*), optional, intent(in) :: title
!---------------------------------------------------------------------------------------------
!  Prints the total number of errors and warnings.
!
!  Inputs:
!
!  . unit (optional): The unit used for printing (default: err_Unit)
!  . reset (optional): if .false., the number of errors and warnings are not reset to 0
!  . title (optional): a title (default: err_title)
!
!  Example:
!    
!   program foo
!    ...
!    call err_resume ( title = '--> foo terminate with', unit = output_unit )
!   end program foo
!-----------------------------------------------------------------------------------R.H. 12/19      

!- local variables: --------------------------------------------------------------------------
   integer                       :: un
   logical                       :: del
   character(len=:), allocatable :: buf, err, warn
!---------------------------------------------------------------------------------------------

   un  = err_Unit ; if ( present(unit)  ) un  = unit
   del = .true.   ; if ( present(reset) ) del = reset
   buf = err_title; if ( present(title) ) buf = title   
   
   err = ' error' ; if ( err_ncall(1) > 1 ) err  = ' errors'
   err = color ( un, err_colorError, err_i2a(err_ncall(1)) // err ) 

   warn = ' warning.' ; if ( err_ncall(2) > 1 ) warn = ' warnings.'
   warn = color ( un, err_colorWarning, err_i2a(err_ncall(2)) // warn )

   buf = buf // ' ' // err // ' and ' // warn
   
   write(un,'(/,a,/)') buf
      
   if ( del ) err_ncall = 0
   
   END SUBROUTINE err_Resume
   
   
!=============================================================================================
   FUNCTION err_constructor ( stat, where, msg, condition ) result ( res )
!=============================================================================================
   integer         , optional, intent(in) :: stat
   character(len=*), optional, intent(in) :: msg, where
   logical         , optional, intent(in) :: condition
   type     (err_t)                       :: res
!---------------------------------------------------------------------------------------------
!  Constructor (see err_set)
!-----------------------------------------------------------------------------------R.H. 12/19      

   call err_set ( res, stat, where, msg, condition ) 
   
   END FUNCTION err_constructor


!=============================================================================================
   SUBROUTINE err_set ( self, stat, where, msg, condition ) 
!=============================================================================================
   class    (err_t),           intent(   out) :: self
   integer         , optional, intent(in    ) :: stat
   character(len=*), optional, intent(in    ) :: msg, where
   logical         , optional, intent(in    ) :: condition
!---------------------------------------------------------------------------------------------
!  Setter
!
!  . if "msg" is present, set self%mesg = msg and self%has_mesg = .true.
!
!  . if "stat" is present, set self%code = stat, else set res%code = err_USAGE  
!
!  . if "where" is present, set sel%loc = where and self%has_loc = .true.  
!
!  . if "condition" is present and .false. return with self%code = 0 (no error)
!
!  Examples:
!
!   subroutine mysub ( ..., flag )
!      ...
!      type(err_t), intent(inout) :: flag
!      ...
!      call flag%Set ( stat = err_USAGE, where = 'mysub', msg = 'Not a valid option' )
!      
!     ! or:
!     ! flag = err_t ( stat = err_USAGE, where = 'mysub', msg = 'Not a valid option' )
!
!      ...
!
!       flag = err_t (  stat = err_USAGE, where = 'mysub', condition = i < 1 .or. i > n, &
!                       msg = 'i is out of range' )
!      ...
!   end subroutine mysub 
!----------------------------------------------------------------------------R.H. 12/19, 12/23      

   if ( present(condition) ) then
      if ( .not. condition ) return
   end if
   
   if ( present(stat) ) self%code = stat
   
   if ( present(msg) ) then
      self%mesg = trim(msg)
      if ( len_trim(msg) > 0 ) self%has_mesg = .true.
   end if
      
   if ( present(where) ) then
      self%loc = trim(where)   
      if ( len_trim(where) > 0 ) self%has_loc = .true.
   end if         

   if ( self%code > 0 ) then
      if ( err_halting ) call err_DisplayErr ( self, unit = err_unit )
      err_ncall(1) = err_ncall(1) + 1
   else if ( self%code < 0 ) then
      if ( err_DispWarn ) call err_DisplayErr ( self, unit = err_unit )
      err_ncall(2) = err_ncall(2) + 1
   end if
   
   END SUBROUTINE err_set
   
   
!=============================================================================================
   pure SUBROUTINE err_AddTrace ( self, trace ) 
!=============================================================================================
   class    (err_t), intent(in out) :: self
   character(len=*), intent(in    ) :: trace
!---------------------------------------------------------------------------------------------
!  Sets self%trace = trace and self_has_trace = .true.
!-----------------------------------------------------------------------------------R.H. 12/19      

   if ( len_trim(trace) > 0 ) then
      if ( self%has_trace ) then
         self%trace = self%trace // ' <<- ' // trim(adjustl(trace))
      else
         self%trace = trim(adjustl(trace))
         self%has_trace = .true.
      end if
   end if
   
   END SUBROUTINE err_AddTrace
   

!=============================================================================================
   pure SUBROUTINE err_AddMsg ( self, msg, before, newline ) 
!=============================================================================================
   class    (err_t),           intent(in out) :: self
   character(len=*),           intent(in    ) :: msg
   logical         , optional, intent(in    ) :: before, newline
!---------------------------------------------------------------------------------------------
!  Adds a message (msg) to self%mesg
!
!  Inputs:
!
!  . msg: the message to be concatenated to self%msg
!
!  . before (optional): if present and .true. msg comes before self%mesg. Otherwise it follows
!                       self%mesg
!
!  . newline (optional): if present and .true. msg is added on a new line
!-----------------------------------------------------------------------------------R.H. 12/19      

!- local variables: --------------------------------------------------------------------------
   logical :: bef, newl
!---------------------------------------------------------------------------------------------

   if ( present(before) ) then
      bef = before
   else
      bef = .false.
   end if
   
   if ( present(newline) ) then
      newl = newline
   else
      newl = .false.
   end if
   
   if ( len_trim(msg) > 0 ) then
      if ( self%has_mesg ) then
         if ( newl ) then
            if ( bef ) then
               self%mesg = trim(msg) // err_NLT // self%mesg
            else
               self%mesg = self%mesg // err_NLT // trim(msg)
            end if
         else
            if ( bef ) then
               self%mesg = msg // self%mesg
            else
               self%mesg = self%mesg // msg
            end if
         end if         
      else
         self%mesg = trim(msg)
         self%has_mesg = .true.
      end if
   end if
   
   END SUBROUTINE err_AddMsg   
   
   
!=============================================================================================
   SUBROUTINE err_DisplayErr ( self, unit, verb, delete, abort, title, trace ) 
!=============================================================================================
   use ansiColor_m, color => ansiColor_colorTxt
   class    (err_t),           intent(in out) :: self
   integer         , optional, intent(in    ) :: unit
   integer         , optional, intent(in    ) :: verb
   logical         , optional, intent(in    ) :: delete, abort, trace
   character(len=*), optional, intent(in    ) :: title
!---------------------------------------------------------------------------------------------
!  Prints self
!
!  By convention: 
!  . self%code = 0 means no error and no warning
!  . self%code < 0 corresponds to a warning
!  . self%code > 0 corresponds to an error
!
!  Optional inputs:
!
!  . unit  : the logical unit of the output file. If not present, the value of the module
!            variable "err_unit" is used.
!
!  . verb  : if verb is present and
!            . verb <= 0: NOTHING IS PRINTED AND SELF IS NOT DELETED
!            . verb > 0 and self%code /= 0 the error or warning message is printed               
!            . moreover if self%code = 0 and self%mesg is not allocated the message
!              'no error, no warning, no message' is printed if verb > 3  
!            if verb is not present, the default value 1 is used
!
!  . delete: by default "self" is deleted unless "delete" is present and equal to .false.  
!
!  . abort : when present and equal to .true., the run is stopped after the print if self%code
!            corresponds to an error (self%code > 0).
!            When not present, err_halting is used.
!
!  . title : a title to print before the warning/error message.
!
!  . trace : when present and equal to .true. print the traceback (self%trace) if any.
!-----------------------------------------------------------------------------------R.H. 12/19      

!- local variables: --------------------------------------------------------------------------
   integer                       :: un, i, verbose
   character(len=:), allocatable :: titre   
   logical                       :: nomsg, del, traceback
   logical         , save        :: firstTime = .true.
   character(len=9), save        :: colorErr, colorWarn, colorOk
!---------------------------------------------------------------------------------------------

   call err_SetErrorCode ()
   
   if ( firstTime .or. colorErr /= err_colorError ) then
      if ( err_colorError(1:2) /= ansiColor_start ) &
         err_colorError = ansiColor_getAnsiCode ( err_colorError )
      colorErr = err_colorError
   end if

   if ( firstTime .or. colorWarn /= err_colorWarning ) then
      if ( err_colorWarning(1:2) /= ansiColor_start ) &
         err_colorWarning = ansiColor_getAnsiCode ( err_colorWarning )
      colorWarn = err_colorWarning
   end if

   if ( firstTime .or. colorOk /= err_colorNormal ) then
      if ( err_colorNormal(1:2) /= ansiColor_start ) &
         err_colorNormal = ansiColor_getAnsiCode ( err_colorNormal ) 
      colorOk = err_colorNormal
   end if
   
   if ( .not. allocated(err_title) ) err_title = ''

   firstTime = .false.
      
   if ( present(verb) ) then
      if ( verb <= 0 ) return ! nothing to do
      verbose = verb
   else
      verbose = 1
   end if

   un        = err_Unit ; if ( present(unit)   ) un        = unit
   del       = .true.   ; if ( present(delete) ) del       = delete
   traceback = err_trace; if ( present(trace)  ) traceback = trace
   titre     = err_title; if ( present(title)  ) titre     = title
   
   nomsg = .true.
   if ( allocated(self%mesg) ) then
      if ( len_trim(self%mesg) /= 0 ) nomsg = .false.
   end if        

   if ( self%has_loc ) then
      if ( .not. allocated(self%loc) ) then
         self%has_loc = .false.
      else
         if ( len_trim(self%loc) == 0 ) self%has_loc = .false.
      end if
   end if 
      
   if ( self%has_trace ) then
      if ( .not. allocated(self%trace) ) then
         self%has_trace = .false.
      else
         if ( len_trim(self%trace) == 0 ) self%has_trace = .false.
      end if
   end if 

   i = self%code
         
   select case ( i )
      case ( 0 ) ! no error
         if ( nomsg ) then
            if ( verbose > 3 ) then
               write(un,*)
               if ( len(titre) > 0 ) write(un,'(a)') color( un,colorOk,titre )
               write(un,'(a,/)')color( un,colorOk,'--> no error, no warning, no message' )
            end if   
         else   
            write(un,*)
            if ( len(titre) > 0 ) write(un,'(a)')color(un,colorOk,titre)
            write(un,'(a,/)')color(un,colorOk,'--> message: '//trim(self%mesg))
         end if

      case ( :-1 ) ! warning or information 
         if ( nomsg ) then
            write(un,*)
            if ( len(titre) > 0 ) write(un,'(a)')color(un,colorWarn,titre)
            write(un,'(a,/)')color(un,colorWarn,trim(err_codes(i)))
         else
            write(un,*)
            if ( len(titre) > 0 ) write(un,'(a)')color(un,colorWarn,titre)
            write(un,'(a)')color(un,colorWarn,trim(err_codes(i))//': '//trim(self%mesg))

            if ( self%has_loc ) &
               write(un,'(a)')color(un,colorWarn,'--> Signaled by: '//trim(self%loc))
            if ( self%has_trace .and. traceback ) &
               write(un,'(a)')color(un,colorWarn,'--> Traceback: '//trim(self%trace))
            write(un,'(a)')' '   
         end if

      case ( 1: ) ! error
         if ( nomsg ) then
            write(un,*)
            if ( len(titre) > 0 ) write(un,'(a)') color(un,colorErr,titre)
            write(un,'(a,/)') color(un,colorErr,trim(err_codes(i)))
         else
            write(un,*)
            if ( len(titre) > 0 ) write(un,'(a)') color(un,colorErr,titre)
            write(un,'(a)') color(un,colorErr,trim(err_codes(i))//': '//trim(self%mesg))

            if ( self%has_loc ) &
               write(un,'(a)') color(un,colorErr,'--> Returned by: '//trim(self%loc)) 
            if ( self%has_trace .and. traceback ) &
               write(un,'(a)') color(un,colorErr,'--> Traceback: '//trim(self%trace)) 
            write(un,'(a)')' '   
         end if
                        
   end select
   
   if ( del ) call self%Destroy()

   if ( i > 0 ) then
      if ( present(abort) ) then
         if ( abort ) then
            write(un,'(a,/)') color(un,colorErr,'--> ABORT (requested)')
            error stop
         end if
      else if ( err_halting ) then
         write(un,'(a,/)') color(un,colorErr,'--> ABORT (Halting Mode enabled)')
         error stop
      end if
   end if
   
   END SUBROUTINE err_DisplayErr
      

!=============================================================================================
   pure FUNCTION err_GetHaltingMode ( ) result ( halting )
!=============================================================================================   
   logical :: halting
!---------------------------------------------------------------------------------------------
!  Get the halting mode
!-----------------------------------------------------------------------------------R.H. 12/19      
   
   halting = err_Halting 
   
   END FUNCTION err_GetHaltingMode
   

!=============================================================================================
   pure FUNCTION err_GetWarningMode ( ) result ( warning )
!=============================================================================================   
   logical :: warning
!---------------------------------------------------------------------------------------------
!  Get the warning mode
!-----------------------------------------------------------------------------------R.H. 12/19      
   
   warning = err_DispWarn 
   
   END FUNCTION err_GetWarningMode
   
   
!=============================================================================================
   pure SUBROUTINE err_MoveAlloc ( from, to ) 
!=============================================================================================
   type(err_t), intent(in out) :: from, to
!---------------------------------------------------------------------------------------------
!  Move from "from" to "to"
!-----------------------------------------------------------------------------------R.H. 12/19      
      
   to%code      = from%code      ; from%code      = 0 
   to%has_trace = from%has_trace ; from%has_trace = .false.
   to%has_loc   = from%has_loc   ; from%has_loc   = .false.
   to%has_mesg  = from%has_mesg  ; from%has_mesg  = .false.
   
   if ( allocated(from%mesg ) ) call move_alloc (from = from%mesg , to = to%mesg )
   if ( allocated(from%trace) ) call move_alloc (from = from%trace, to = to%trace)
   if ( allocated(from%loc  ) ) call move_alloc (from = from%loc  , to = to%loc  )

   END SUBROUTINE err_MoveAlloc 

   
!=============================================================================================
   pure SUBROUTINE err_DestroyErr ( self ) 
!=============================================================================================
   class(err_t), intent(in out) :: self
!---------------------------------------------------------------------------------------------
!  Destroys self
!-----------------------------------------------------------------------------------R.H. 12/19      

   self%code = 0
   if ( allocated(self%mesg ) ) deallocate(self%mesg ) ; self%has_mesg  = .false.
   if ( allocated(self%trace) ) deallocate(self%trace) ; self%has_trace = .false.
   if ( allocated(self%loc  ) ) deallocate(self%loc  ) ; self%has_loc   = .false.

   END SUBROUTINE err_DestroyErr 


!=============================================================================================
   pure SUBROUTINE err_FinalizeErr ( self ) 
!=============================================================================================
   type(err_t), intent(in out) :: self
!---------------------------------------------------------------------------------------------
!  Finalizer
!-----------------------------------------------------------------------------------R.H. 12/19      

   self%code = 0
   if ( allocated(self%mesg ) ) deallocate(self%mesg ) ; self%has_mesg  = .false.
   if ( allocated(self%trace) ) deallocate(self%trace) ; self%has_trace = .false.
   if ( allocated(self%loc  ) ) deallocate(self%loc  ) ; self%has_loc   = .false.

   END SUBROUTINE err_FinalizeErr 


!=============================================================================================
   pure FUNCTION err_Eq ( a, b ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a, b
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code == b%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code == b%code)
   
   END FUNCTION err_Eq
   

!=============================================================================================
   pure FUNCTION err_EqIntg ( a, i ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code == i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code == i)
   
   END FUNCTION err_EqIntg


!=============================================================================================
   pure FUNCTION err_IntgEq ( i, a ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code == i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code == i)
      
   END FUNCTION err_IntgEq


!=============================================================================================
   pure FUNCTION err_Ne ( a, b ) result( res )
!=============================================================================================
   class(err_t), intent(in) :: a, b
   logical                  :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code == b%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code /= b%code)
   
   END FUNCTION err_Ne
   

!=============================================================================================
   pure FUNCTION err_NeIntg ( a, i ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code == i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code /= i)
   
   END FUNCTION err_NeIntg


!=============================================================================================
   pure FUNCTION err_IntgNe ( i, a ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code /= i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code /= i)
      
   END FUNCTION err_IntgNe


!=============================================================================================
   pure FUNCTION err_Gt ( a, b ) result( res )
!=============================================================================================
   class(err_t), intent(in) :: a, b
   logical                  :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code > b%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code > b%code)
   
   END FUNCTION err_Gt
   

!=============================================================================================
   pure FUNCTION err_GtIntg ( a, i ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code > i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code > i)
   
   END FUNCTION err_GtIntg


!=============================================================================================
   pure FUNCTION err_IntgGt ( i, a ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if i > a%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (i > a%code)
      
   END FUNCTION err_IntgGt


!=============================================================================================
   pure FUNCTION err_Ge ( a, b ) result( res )
!=============================================================================================
   class(err_t), intent(in) :: a, b
   logical                  :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code >= b%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code >= b%code)
   
   END FUNCTION err_Ge
   

!=============================================================================================
   pure FUNCTION err_GeIntg ( a, i ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code >= i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code >= i)
   
   END FUNCTION err_GeIntg


!=============================================================================================
   pure FUNCTION err_IntgGe ( i, a ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if i >= a%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (i >= a%code)
      
   END FUNCTION err_IntgGe


!=============================================================================================
   pure FUNCTION err_Lt ( a, b ) result( res )
!=============================================================================================
   class(err_t), intent(in) :: a, b
   logical                  :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code < b%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code < b%code)
   
   END FUNCTION err_Lt
   

!=============================================================================================
   pure FUNCTION err_LtIntg ( a, i ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code < i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code < i)
   
   END FUNCTION err_LtIntg


!=============================================================================================
   pure FUNCTION err_IntgLt ( i, a ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if i < a%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (i < a%code)
      
   END FUNCTION err_IntgLt


!=============================================================================================
   pure FUNCTION err_Le ( a, b ) result( res )
!=============================================================================================
   class(err_t), intent(in) :: a, b
   logical                  :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code <= b%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code <= b%code)
   
   END FUNCTION err_Le
   

!=============================================================================================
   pure FUNCTION err_LeIntg ( a, i ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if a%code <= i
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (a%code <= i)
   
   END FUNCTION err_LeIntg


!=============================================================================================
   pure FUNCTION err_IntgLe ( i, a ) result( res )
!=============================================================================================
   class  (err_t), intent(in) :: a
   integer       , intent(in) :: i
   logical                    :: res
!---------------------------------------------------------------------------------------------
!  Returns .true. if i <= a%code
!-----------------------------------------------------------------------------------R.H. 12/19      

   res = (i <= a%code)
      
   END FUNCTION err_IntgLe


!=============================================================================================   
   pure FUNCTION err_i2a ( i ) result( res )
!=============================================================================================
   integer         ,             intent(in) :: i
   character(len=:), allocatable            :: res
!---------------------------------------------------------------------------------------------
!  Converts integer to string (from Vladimir F., https://stackoverflow.com)
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=range(i)+2) :: tmp
!---------------------------------------------------------------------------------------------

   write(tmp,'(i0)') i ;  res = trim(tmp)
  
  END FUNCTION err_i2a
  
         
END MODULE err_m
