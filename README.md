# A very simple Fortran error handler: err_m

An error message like:

![](media/fig1.png)

might be sufficient for a small-scale program. However, for a substantial project with complex nested sequences, it is not very helpful because:

- it does not provide information about the path that led to this error,
- it does not transfer to the calling procedure the decision on the action to take.

An error handler should give the user the choice to either halt the program as soon as an error occurs or to transfer this decision to the calling procedure(s). Considere for example the message:

![](media/fig2.png)

It clearly indicates that the SolveLin procedure was called by Newton, which in turn was called by ComputeJacMat, and so on, until the Models procedure ultimately decided to halt the execution.

1. [What is err_m?](#err)
2. [Choosing the halting mode](#haltingMode)
3. [Setting an error or a warning](#seterr)
4. [Checking for an error and displaying its message](#diplay)
5. [Setting your own error/warning codes and their meanings](#codes)
6. [Recording procedure calls (trace)](#trace)
7. [Adding further information to an existing message](#addMsg)
8. [Customizing the error handler](#custom)
9. [Using macros (cpp)](#macros)
10. [Examples](#examples)


## 1.  What is err_m? <a name="err"></a>

`err_m` is a small module that defines a derived type named `err_t`, which facilitates the straightforward handling of error and warning notifications. A variable of type `err_t` has the following private members 

+ `%code`: (integer) positive for an error, negative for a warning
+ `%mesg`: (character) the error or warning message 
+ `%loc`: (character) the name of the procedure where it was defined (if appropriate)
+ `%trace`: (character) a list of potential procedures that called the procedure `%loc`

To use `err_m`, you need to include the use statement in your program or modules:

```fortran
use err_m
```

Then, declare one or more variables of type `err_t`, like:

```fortran
type(err_t) :: flag
```

Pass this variable to your various procedures either as an argument or as a module variable.


## 2.  Choosing the halting mode <a name="haltingMode"></a>

You can define the behavior of your code when an error occurs (stop just after reporting it or delegate the decision to the rest of the program) by calling the `err_SetOptions` procedure (usually at the beginning of the code, but you can call it again at any time to change the mode):

+	`call err_SetOptions ( halting = .true. ) !< (not really useful, halting = .true. being the default)`

In this case, as soon as an error is set (see [(3)](#seterr) below), the error message is displayed, and the program is halted.

+	`call err_SetOptions ( halting = .false. ) !< disable automatic halting`

In this case, assigning an error does not stop the program, and it is up to the rest of the program to take the necessary steps to handle the error.

Note however that a warning cannot be postponed to the caller: its message will be immediately displayed (or ignored depending on the option chosen (see [(8)](#custom)). 


## 3.  Setting an error or a warning <a name="seterr"></a>

To set an error, you can use either the type-bound procedure `%Set` or the defined constructeur `err_t`. Here is an example of an error setting in a procedure `mysub`:

```fortran
if ( i < 0 .or. i > nmax ) then
   call flag%Set ( stat = codeError, where = 'mysub', msg = 'Index out of bounds' )
   return
end if
```
or  
    
```fortran
if ( i < 0 .or. i > nmax ) then
   flag = err_t ( stat = codeError, where = 'mysub', msg = 'Index out of bounds' )
   return
end if
```   

These two procedures have four optional arguments with the following meaning: 

- `stat`: (integer) an error/warning code, positive for an error, negative for a warning
- `where`: (character) the name of the procedure where the error/warning holds 
- `msg`: (character) the message to display 
- `condition`: (logical) a logical expression (if false, means no error or warning). Can be useful when using a macro to shorten the above lines (see [(9)](#macros)). 


## 4.  Checking for an error and displaying its message <a name="display"></a>

If automatic halting mode is enabled (see [(2)](#haltingMode)) error messages are directly displayed (on the choosen unit) and the program is stopped. Otherwise it's up to you to display them and decide how to proceed: 

```fortran
call foo ( flag )
! check if foo has returned an error:
if ( flag > 0 ) then
   ! display here the message and
   ! decide to stop or to continue/to return
end if
```

You can check for any error (`flag > 0`) or for a given error if you know its code (see [(5)](#codes)). Note that comparison operators (`>`, `<`, `>=`, `<=`, `==`, `/=`) are overloaded. 

Messages are printed by calling the type-bound procedure `%Display`:

```fortran
call flag%Display()
```

which has no effect if there is no error (`flag%code = 0`). This subroutine can take several arguments, the main ones being:

+ `abort`: (logical) if `.true.` and if `flag%code > 0` the program is stopped after the print (the default is: `abort = .false.`)
+ `unit`: (integer) the logical unit of the output file (the default is: `unit = err_unit` the module variable initialized to stdout and modifiable by the user)
+ `delete:`: (logical) if `.true.` reset `flag` after the print (the default is: `delete = .true.`)
+ `trace:` (logical) if `.true.` print also the traceback i.e. `flag%trace` (the default is: `trace = err_trace` the module variable initialized to `.true.` and modifiable by the user)

Here is a very simple example (see `arcLength.f90` in the subdirectory `examples/` )

```fortran
program arcLength
   use err_m
   implicit none
   type(err_t) :: errFlag
   real        :: radius, angle, length
   
   call err_SetOptions ( halting = .false. )  
           
   write(*,'(a)',advance='no') 'Give the radius and the angle (in deg.): '
   read(*,*) radius, angle

   call circularArcLength ( radius, angle, errFlag, length )
   call errFlag%display()

   !...
   !...
   !...
      
contains
   subroutine circularArcLength ( radius, angle, flag, length )
      real       , intent(in    ) :: radius, angle
      type(err_t), intent(in out) :: flag
      real       , intent(   out) :: length

      character(len=*), parameter :: HERE = 'circularArcLength'
      real                        :: alpha
    
      length = 0.0
   
      if ( radius < 0.0 ) then
         flag = err_t ( stat = err_USAGE, where = HERE, msg = 'Negative radius' )
         return
      end if
    
      alpha = abs(angle)

      if ( alpha > 360.0 ) then
         flag = err_t ( stat = err_WARNING, where = HERE, &
                         msg = 'Absolute value of angle exceed 360 degrees' // err_NLT // &
                               'We use its principal measure')

         alpha = alpha - (nint(alpha)/360) * 360.0
         if ( alpha == 0.0 ) alpha = 360.0
      end if

      length = alpha * radius * atan(1.0) / 45.0
   end subroutine circularArcLength 

end program arcLength   
```

In this example, when the radius is negative, the error message is displayed by the main but the program is not stopped.


## 5.  Setting your own error/warning codes and their meanings <a name="codes"></a>

The module err_m contains very few public predefined error/warning codes: 

+ `err_USAGE=1` for a user/usage error
+ `err_READ=2` for a read error
+ `err_ALLOC=3` for an allocation or deallocation problem 
+ `err_INTERNAL=4` for an internal error
+ `err_WARNING=-1` for a general warning 

If you are not happy with these definitions you can change them or you can add your own codes and definitions by calling the subroutine `err_SetErrorCode` anywhere in your code.

Example:
```fortran
integer, parameter :: ERR1 = 1, ERR2 = 5, STUPID = 6

!...

call err_SetErrorCode ( code = ERR1, definition = 'Invalid option')
call err_SetErrorCode ( code = ERR2, definition = 'Singular system')
call err_SetErrorCode ( code = STUPID, definition = 'Stupid error')

!...
```


## 6.  Recording procedure calls (trace) <a name="trace"></a>

In order to record the procedure calls, each caller can add its name to the traceback by using the type-bound procedure `%AddTrace`.

Example:

```fortran
subroutine subfoo1 ( ... , flag )
   !...
   type(err_t), intent(in out) :: flag
   character(len=*), parameter :: HERE = 'subfoo1'

   !...
   call subfoo2 ( ... , flag )

   if ( flag > 0 ) then
      ! An error has been reported by subfoo2. 
      ! subfoo1 adds its name to the trace and decides to return:
      call flag%AddTrace ( HERE )
      return
   end if

!...
   
end subroutine subfoo1

subroutine subfoo2 ( ... , flag )
   !...
   type(err_t), intent(in out) :: flag
   character(len=*), parameter :: HERE = 'subfoo2'

   !...
   call subfoo3 ( ... , flag )

   if ( flag > 0 ) then
      ! An error has been reported by subfoo3. 
      ! subfoo2 adds its name to the trace and decides to return:
      call flag%AddTrace ( HERE )
      return
   end if
   
!...

end subroutine subfoo2
   
!...
```

## 7.  Adding further information to an existing message <a name="addMsg"></a>

It may be useful for a caller to add (to concatenate) a message to the one returned by a called procedure. This can be done by the type-bound subroutine `%AddMsg`. For example:
 
```Fortran
subroutine foo ( ... , flag )
   type(err_t), intent(in out) :: flag
   !...

   character(len=*), parameter :: HERE = 'foo'
   !...
   do i = 1, size(x)
      z(i) = funcf ( x(i), flag )
      if ( flag > 0 ) then
         j = i
         exit
      end if
   end do

   if ( flag > 0 ) then
      ! Example of adding further information to an existing message:
      call flag%addMsg ( 'Error occurred for index '//int2char(j)//' of "x" in '// &
                         'the main loop of '//HERE, newline = .true. )
      ! Add 'foo' to the trace and return:
      call flag%addTrace ( HERE )
      return
   end if
```
The `%AddMsg` subroutine has two other (optional) arguments: 
+ `newline`: (logical) if present and .true. the message is added on a new line.
+ `before`: (logical) if present and .true. the message to be added comes before the existing one. Otherwise, it follows it.


## 8.  Customizing the error handler <a name="custom"></a>

You can override some default options by calling the `err_SetOptions` subroutine which has the following optional arguments:

+ `halting`: (logical) halting mode (see also [(2)](#haltingMode)). The default is `.true.`
+ `ieee_halting`: (logical) halting mode for ieee exception. The default is `.false.`
+ `DisplayWarning`: (logical) if set to `.false.` warnings are not printed. The default is `.true.`
+ `unit`: (integer) the logical unit for printing the error/warning message. The default is the sdtout.
+ `title`: (character) a general title. The default is an empty line.
+ `trace`: (logical) if set to `.true.` traceback is printed. The default is `.true.`. 
+ `errorColor`: (character) the color code for an error message. The default is `'rbh'` (see below).
+ `warnColor`: (character) the color code for a warning message. The default is `'gbh'` (see below).
+ `normalColor`: (character) the color code for other message. The default is `'bbh'` (see below).

Note that you can call this subroutine as many times as you like to change one or more of these options

### Color codes

The first character of a color code must be `'b'` (blue), `'c'` (cyan), `'g'` (green), `'k'` (black), `'m'` (magenta), `'r'` (red) or `'y'` (yellow). Append `'b'` for bold and/or `'h'` for high intensity.


## 9.  Using macros (cpp) <a name="macros"></a>

The block of lines  

```fortran
if ( myflag > 0 ) then
   call myflag%AddTrace ( 'mysub' )
   return
end if
```

can be advantageously shortened to a single line by using the macro `error_TraceNreturn` defined in the file `error.fpp`: 

```fortran
error_TraceNreturn ( myflag, 'mysub' )
```

To this end, include the file `error.fpp` (`#include "error.fpp"`) at the beginning of the files containing your program or modules that use this macro and compile with the flag `-cpp` (gfortran) or `-fpp` (ifort, nagfor).

Similarly, the macro `error_TraceDisplayNStop`: 
```fortran
error_TraceDisplayNStop ( myflag, 'mysub' )
```
can be used in place of the block

```fortran
if ( myflag > 0 ) then
   call myflag%AddTrace ( 'mysub' )
   call myflag%Display ( abort = .true. )
end if
```

And finally, to shorten the set of lines

```fortran
if ( mycondition ) then
   myflag = err_t ( stat = mycode, where = 'mysub', msg = 'mymessage' )
   return
end if
```

one can use the macro `error_SetNreturn`: 
```fortran
error_SetNreturn ( mycondition, myflag, mycode, 'mysub', 'mymessage' )
```
      
**Warning:** note however that cpp directives are not standardized and some fortran symbols like continuation line (&), comments (!), etc may not be recognized by the preprocessor. Unfortunately this is the case with gfortran (which use the "traditional-cpp"). For portability reasons, it is not recommended to use this last macro, unless the messages are short enough. 


## 10.  Examples <a name="examples"></a>

Some examples can be found in the subdirectory `examples/`. Here is for example the output of one of them:

![](media/fig3.png)
